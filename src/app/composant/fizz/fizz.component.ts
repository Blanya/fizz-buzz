import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fizz',
  templateUrl: './fizz.component.html',
  styleUrls: ['./fizz.component.scss']
})
export class FizzComponent implements OnInit {

  count: number = 0;
  isNumber: boolean = true;
  text: string = "";
  typo : string = "";

  constructor() { }

  moins()
  {
    this.count--;
    this.verif();
  }

  plus()
  {
    this.count++;
    this.verif();
  }

  verif()
  {
    this.count%15 ==0 ? (this.text = "FizzBuzz", this.isNumber = false) : 
    this.count%3 == 0 ? (this.text = "Fizz", this.isNumber = false) : 
    this.count%5 == 0 ? (this.text = "Buzz", this.isNumber = false) : this.isNumber= true;

    this.isNumber? this.typo = "fw-light" : this.typo = "fw-bold fs-3 font-monospace";
  }

  ngOnInit(): void {
  }

}
